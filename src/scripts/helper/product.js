import fetch from 'node-fetch';

export function getProduct(productHandle) {
  return fetch(`/products/${productHandle}.js`, {
    method: 'GET',
  }).then((response) => {
    return response.json();
  }).then((json) => {
    const productEvent = new window.CustomEvent('product:get', {
      detail: {
        json,
      },
    });

    document.dispatchEvent(productEvent);
    return product;
  });
}
