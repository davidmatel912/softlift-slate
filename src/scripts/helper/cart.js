import fetch from 'node-fetch';

export function getCart(cart) {
  return fetch('/cart.js', {
    method: 'GET',
  }).then((response) => {
    return response.json();
  }).then((json) => {
    const cartEvent = new window.CustomEvent('cart:get', {
      detail: {
        json,
      },
    });

    document.dispatchEvent(cartEvent);
    return cart;
  });
}

export function addItem(data) {
  fetch('/cart/add.js', {
    body: JSON.stringify(data),
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    },
    method: 'POST',
  }).then((response) => {
    return response.json();
  }).then((json) => {
    const cartEvent = new window.CustomEvent('cart:added', {
      detail: {
        json,
      },
    });

    document.dispatchEvent(cartEvent);
    return json;
  }).catch((error) => {
    const cartEvent = new window.CustomEvent('cart:error', {
      detail: {
        error,
      },
    });

    document.dispatchEvent(cartEvent);
    return error;
  });
}

export function updateCart(data) {
  fetch('/cart/update.js', {
    body: JSON.stringify(data),
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    },
    method: 'POST',
  }).then((response) => {
    return response.json();
  }).then((json) => {
    const cartEvent = new window.CustomEvent('cart:update', {
      detail: {
        json,
      },
    });

    document.dispatchEvent(cartEvent);
    return json;
  }).catch((error) => {
    const cartEvent = new window.CustomEvent('cart:error', {
      detail: {
        error,
      },
    });

    document.dispatchEvent(cartEvent);
    return error;
  });
}

export function updateItem(data) {
  fetch('/cart/change.js', {
    body: JSON.stringify(data),
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    },
    method: 'POST',
  }).then((response) => {
    return response.json();
  }).then((json) => {
    const cartEvent = new window.CustomEvent('cart:change', {
      detail: {
        json,
      },
    });

    document.dispatchEvent(cartEvent);
    return json;
  }).catch((error) => {
    const cartEvent = new window.CustomEvent('cart:error', {
      detail: {
        error,
      },
    });

    document.dispatchEvent(cartEvent);
    return error;
  });
}

export function clearItems() {
  return fetch('/cart/clear.js', {
    method: 'POST',
  }).then((response) => {
    return response.json();
  }).then((json) => {
    const cartEvent = new window.CustomEvent('cart:clear', {
      detail: {
        json,
      },
    });

    document.dispatchEvent(cartEvent);
    return json;
  });
}

export function getShippingRates(address) {
  const addressString = Object.keys(address).map((key) => {
    return `${encodeURIComponent(key)}=${encodeURIComponent(address[key])}`;
  }).join('&');

  return fetch(`/cart/shipping_rates.json?${addressString}`, {
    method: 'GET',
  }).then((response) => {
    return response.json();
  }).then((json) => {
    const cartEvent = new window.CustomEvent('cart:get:rates', {
      detail: {
        json,
      },
    });

    document.dispatchEvent(cartEvent);
    return json;
  });
}
