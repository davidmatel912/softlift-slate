export function responsiveTables() {
  const rteTables = document.querySelectorAll('.rte table');

  [].forEach.call(rteTables, (table) => {
    table.classList.add('responsive-table');
  });
}
